import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class VentanaJuego extends JFrame {

    private PanelVirus panelVirus = new PanelVirus();

    public VentanaJuego() {
        setBounds(600, 300, 800, 450);
        setTitle("Virus inminente");
        JPanel botonera = new JPanel();
        JButton boto1 = new JButton("Pandemia");
        JButton boto2 = new JButton("Salir");
        botonera.add(boto1);
        botonera.add(boto2);
        boto1.addActionListener(new ClickBotoPilotaVa());
        boto2.addActionListener(new ClickSortir());
        add(botonera, BorderLayout.SOUTH);
    }

    class ClickSortir implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            System.exit(0);
        }
    }

    class ClickBotoPilotaVa implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            panelVirus.addVirus();
        }
    }
}