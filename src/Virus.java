import javax.swing.*;
import java.awt.*;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Rectangle2D;
import java.net.URL;

public class Virus extends Thread {

    private double isAlive;
    private double x = 0;
    private double y = 0;
    private double dx = 1;
    private double dy = 1;
    private int radio = 15;

    private JPanel elPanelComu;
    private final URL urlVirus = getClass().getClassLoader().getResource("images/virus.jpg");
    private final URL urlVirusDie = getClass().getClassLoader().getResource("images/virusDie.jpg");
    private final Image image = new ImageIcon(urlVirus).getImage();
    private final Image imageVirusDie = new ImageIcon(urlVirusDie).getImage();

    public void setElPanelComu(JPanel p) {
        elPanelComu = p;
    }

    public void moureVirus() {
        Rectangle2D limit = elPanelComu.getBounds();
        double width = limit.getWidth();
        double height = limit.getHeight();
        x += dx;
        y += dy;
        if (x + radio / 2 > width || x + radio / 2 < 0) dx = -dx;
        if (y + radio / 2 > height || y + radio / 2 < 0) dy = -dy;
    }

    public Ellipse2D dibujaVirus() {
        return new Ellipse2D.Double(x, y, radio, radio);
    }

    public void matar(){

    }

    @Override
    public void run() {
        for (int i = 1; i <= 2000; i++) {
            moureVirus();
            try {
                Thread.sleep(4);
            } catch (Exception e) {
            }
            elPanelComu.repaint();
        }
    }
}