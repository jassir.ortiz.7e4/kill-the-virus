import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.awt.Graphics2D;

class PanelVirus extends JPanel implements MouseListener {
    private ArrayList<Virus> virus = new ArrayList<Virus>();

    public void addVirus() {
        Virus p = new Virus();
        virus.add(p);
        p.start();
    }

    public void paint(Graphics g) {
        Graphics2D g2 = (Graphics2D) g;
        for (Virus p : virus) {
            g2.fill(p.dibujaVirus());
        }
        Toolkit.getDefaultToolkit().sync();
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        int x = e.getX();
        int y = e.getY();
    }

    @Override
    public void mousePressed(MouseEvent e) {

        /**
         * Fent aquest sistema em permet parar el virus al donar-me les coordenades de la seva posició
         */
        int xNegative = e.getX()-60;
        int x = e.getX()+60;
        int y = e.getY()+60;
        int yNegative = e.getY()-60;


        Toolkit.getDefaultToolkit().sync();
    }

    @Override
    public void mouseReleased(MouseEvent e) {

    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }
}